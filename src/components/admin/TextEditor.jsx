// import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
// import { CKEditor } from '@ckeditor/ckeditor5-react';
// import React from 'react';

// const TextEditor = ({initData, setData}) => {

//     const EditorConfig = {
// 		toolbar: {
// 			items: [
// 				'heading',
// 				'|',
// 				'bold',
// 				'italic',
// 				'link',
// 				'bulletedList',
// 				'numberedList',
// 				'|',
// 				'outdent',
// 				'indent',
// 				'|',
// 				'undo',
// 				'redo',
// 				'imageUpload',
// 				'insertTable',
// 				'accessibilityHelp',
// 				'mediaEmbed',
// 				'ckbox',
// 				'blockQuote'
// 			]
// 		},
// 		language: 'km',
// 		image: {
// 			toolbar: [
// 				'imageTextAlternative',
// 				'toggleImageCaption',
// 				'imageStyle:inline',
// 				'imageStyle:block',
// 				'imageStyle:side'
// 			]
// 		},
// 		table: {
// 			contentToolbar: [
// 				'tableColumn',
// 				'tableRow',
// 				'mergeTableCells'
// 			]
// 		}
// 	};


//     return (
//         <div>
//             <CKEditor
//                 editor={ClassicEditor}
//                 config={EditorConfig}
//                 data={initData}
//                 onChange={(event, editor) => {
//                     const data = editor.getData();
//                     console.log(data);
//                     setData(data);
//                 }}
//             >

//             </CKEditor>
//         </div>
//     );
// }

// export default TextEditor;


import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import axios from 'axios';
import React, { useEffect, useState } from 'react';

const TextEditor = () => {

    const [formData, setFormData] = useState({
        description: '',
      });
    
      const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData(prevState => ({
          ...prevState,
          [name]: value
        }));
      };
    
      const handleSubmit = (e) => {
        e.preventDefault();
        axios.post('/api/storeText', formData) // Assuming your API endpoint is /api/store
          .then(response => {
            // Handle success, maybe show a success message or redirect
            console.log(response.data);
          })
          .catch(error => {
            // Handle error, maybe show an error message
            console.error(error);
          });

        
      };

    return (
        <div>
            <form onSubmit={handleSubmit} className="form form-horizontal">
            
            <div className="form-group" id='summary-ckeditor'>
                <label>Description</label>
                <CKEditor
                    editor={ClassicEditor}
                    data={formData.description}
                    onChange={(event, editor) => {
                        const data = editor.getData();
                        setFormData({ ...formData, description: data });
                    }}
                    config={{
                        ckfinder: {
                            uploadUrl: "http://127.0.0.1:8000/api/upload"
                        }
                    }}
                />
            </div>
            
            <div className="form-group">
                <input type="submit" value="Submit" className="btn btn-primary"/>
            </div>
        </form>
        </div>
    );
}

export default TextEditor;
