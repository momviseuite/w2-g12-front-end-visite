import axios from 'axios';
import React, { useEffect, useState } from 'react'

export default function ReviewComment() {

  const [comments, setComments] = useState([]);

  useEffect(() =>{

    // comments
    axios.get('api/comments').then(res=>{
      if(res.data.status === 200){
          setComments(res.data.message);
      }
      // setLoading(false);
    })
  
  },[])

  return (
    <div className='container'>
        {
          comments.map((item) => {
              return (
                  <div key={item.id}>
                      <div class="card p-3 mt-2">
                          <div class="d-flex justify-content-between align-items-center">
                              <div class="user d-flex flex-row align-items-center">
                                  <img src="https://i.imgur.com/C4egmYM.jpg" width="30" class="user-img rounded-circle mr-2"/>
                                  <span><small class="font-weight-bold text-primary">{item.user.name}</small></span>
                              </div>
                              <small>{new Date(item.created_at).toLocaleString()}</small>
                          </div>

                          <div class="action d-flex justify-content-between mt-2 align-items-center">
                              <div class="reply px-4">
                                  <small class="font-weight-bold">{item.body}</small>
                              </div>

                              
                          </div>
                      </div>

                  </div>
              )
          })
          }
    </div>
  )
}
