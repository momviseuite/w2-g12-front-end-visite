import axios from 'axios';
import React, { useEffect, useState } from 'react';
import Ratings from '../frontend/collections/Ratings';
import { Link } from 'react-router-dom';

const ReviewRating = () => {

    const [ratings, setRatings] = useState([]);

    useEffect(() =>{

        // ratting
        axios.get('/api/ratings')
            .then(response => {
                setRatings(response.data);
            })
            .catch(error => {
                console.error(error);
            });
      
      },[]) 

    return (
        <div className='container'>
        {
          ratings.map((item) => {
              return (
                  <div key={item.id}>
                      <div class="card p-3 mt-2">
                          <div class="d-flex justify-content-between align-items-center">
                              <div class="user d-flex flex-row align-items-center">
                                  <img src="https://i.imgur.com/C4egmYM.jpg" width="30" class="user-img rounded-circle mr-2"/>
                                  <span><small class="font-weight-bold text-primary">{item.user.name}</small></span>
                              </div>
                            <small>
                                {new Date(item.created_at).toLocaleString()}
                            </small>
                              
                          </div>

                          <div class="action d-flex justify-content-between mt-2 align-items-center">
                              <div class="reply px-4">
                                  <small class="font-weight-bold">
                                  <div class="font-medium">
                                    <img src={`https://visit-place.shop/public/${item.address.image}`} class="card-img-top box-pro" alt="..." />
                                    <p>{item.address.name}</p>
                                    <Link to={`/Demo_address/${item.address.id}/${item.address.name}`} className=''>See Detail</Link>
                                </div>
                                    <div className="loc-pri-flex">
                                        <p> Rating: ({item.rating.toFixed(2)})</p>
                                        <p><Ratings value={item.rating} /> </p>
                                    </div>
                                  </small>
                              </div>

                              
                          </div>
                      </div>

                  </div>
              )
          })
          }

        {/* {ratings.filter(rating => rating.address_id === item.id).map(rating => (
            <div key={rating.id}>
                <div className="loc-pri-flex">
                    <p> Rating: ({rating.rating.toFixed(2)})</p>
                    <p><Ratings value={rating.rating} /> </p>
                </div>
                <hr className=' hr-size' />
            </div>
        ))} */}
    </div>
    );
}

export default ReviewRating;
