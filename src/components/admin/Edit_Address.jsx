import React, { useEffect, useState } from 'react';
import GoogleMapReact from 'google-map-react';
import axios from 'axios';
import Swal from 'sweetalert2';
import { useNavigate, useParams } from 'react-router-dom';

const EditAddress = () => {
    const navigate = useNavigate();
    const {id} = useParams();    
    const [loading, setLoading] = useState(true);
    const [validated, setValidated] = useState(false);
    const [allCategory, setAllCategory] = useState([]);
    const [province, setProvince] = useState([]);
    const [errorList,setError] = useState([]);
    const [addressInput, setAddressInput] = useState({
        name: '',
        category_id: '',
        province_id: '',
        image: '',
        type_address: '',
        key_points: '',
        content: '',
        gallery: '',
        description: '',
        featured: '',
        status: '',
        
    });
    const [picture, setPicture] = useState([]);

    const handleInput = (e) => {
        e.persist();
        setAddressInput({...addressInput, [e.target.name]: e.target.value});
    }

    const[errorImage, setErrorImage] = useState(null);
    const handleImage = (e) =>{
        
        const selectedFile = e.target.files[0];
        const maxSize = 3 * 1024 * 1024; // 2MB
    
        if (selectedFile.size <= maxSize) {
            setPicture({image: e.target.files[0]});
            setErrorImage(null);
        } else {
            setErrorImage(' maximum size of 3MB.');
        }
    }

    const [allCheckbox, setAllCheckbox] = useState([]);
    const handleChecked = (e) =>{
        setAllCheckbox({...allCheckbox, [e.target.name]:e.target.checked});
    }

    useEffect(() =>{        

        // api category
        axios.get('api/all-category')
            .then(res => {
                if (res.data.status === 200) {
                    setAllCategory(res.data.category);
                }
            })
            .catch(error => {
                console.error('Error fetching categories:', error);
            });

        // api province
        axios.get('api/province')
        .then(res => {
            if (res.data.status === 200) {
                setProvince(res.data.province);
            }
        })
        .catch(error => {
            console.error('Error fetching provinces:', error);
        });

        

        // get id
        axios.get(`api/edit-address/${(id)}`).then(res=>{
            if(res.data.status === 200){
                setAddressInput(res.data.message);
                setAllCheckbox(res.data.message);
            }
            
            else if(res.data.status === 400){
                Swal.fire({
                    title: "Warning",
                    text: res.data.message,
                    icon: "warning"
                });
                navigate("/admin/view-addressInput");
            }
            setLoading(false);
        });


    },[id]);

    const submitaddressInput = (e) => {
        e.preventDefault();
        const form = e.target;
    
        //check validate style
        if (!form.checkValidity()) {
        e.stopPropagation();
        }
        setValidated(true);

        const formData =  new FormData();
        formData.append('image', picture.image);
        formData.append('category_id', addressInput.category_id);
        formData.append('province_id', addressInput.province_id);
        formData.append('type_address', addressInput.type_address);
        formData.append('name', addressInput.name);
        formData.append('key_points', addressInput.key_points);
        formData.append('content', addressInput.content);

        formData.append('description', addressInput.description);
        formData.append('featured', allCheckbox.featured ? 1 : 0);
        formData.append('status', addressInput.status ? 1 : 0);

        axios.post(`api/update-address/${id}`, formData,
            {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }
        )
        .then(res => {
            if(res.data.status === 200){
                
                Swal.fire({
                    title: "Success",
                    text: res.data.message,
                    icon: "success"
                });
                setError([]);
            }
            else if(res.data.status === 400){
                setError(res.data.validation_error);
            }
        })

    };

    
    
    
    if(loading){
        return (
            <>
                <h4>Loading edit addressInput...</h4>
            </>
        )
    } 
    


    

    return (
        <div>
            <div class="container-fluid">
                <form id="myForm" onSubmit={submitaddressInput}  className={`row g-3 needs-validation ${validated ? 'was-validated is-valid' : ''}`} noValidate>
                    
                <div class="col-md-4">
                        <label for="name">Name:</label>
                        <input type="text" class="form-control" name='name' onChange={handleInput} value={addressInput.name} id="name" required />
                        <div class="invalid-feedback"> {errorList.name}</div>
                    </div>

                    <div class="col-md-4">
                        <label for="category_id" class="form-label">Category</label>
                        <select class="form-select " name='category_id' onChange={handleInput} value={addressInput.category_id} id="category_id" aria-describedby="validationServer04Feedback" required >                            
                            <option value="">Select a category</option>
                                {
                                    allCategory.map((item) =>{
                                        return (
                                            <option key={item.id} value={item.id} >{item.name_place}</option>
                                        )
                                    })
                                }
                            
                        </select>
                        <div id="validationServer04Feedback" class="invalid-feedback">{errorList.category_id}</div>
                    </div>

                    <div class="col-md-4">
                        <label for="province_id" class="form-label">Province</label>
                        <select class="form-select " name='province_id' onChange={handleInput} value={addressInput.province_id} id="province_id" aria-describedby="validationServer04Feedback" required >                            
                            <option value="">Select a province</option>
                                {
                                    province.map((item) =>{
                                        return (
                                            <option key={item.id} value={item.id} >{item.name}</option>
                                        )
                                    })
                                }
                        </select>
                        <div id="validationServer04Feedback" class="invalid-feedback">{errorList.province_id}</div>
                    </div>
                    
                  

                    <div class="col-md-4">
                        <label for="key_points">Key Points:</label>
                        <input type="text" class="form-control" name='key_points' onChange={handleInput} value={addressInput.key_points} id="key_points"  required />
                        <div class="invalid-feedback">{errorList.key_points}</div>
                    </div>

                    <div class="col-md-4">
                        <label for="image" class="form-label">image:</label>
                        <input type="file" id='image' name='image' onChange={handleImage}  class="form-control" />
                        <img className='mt-2' width={80} height={80} src={`https://visit-place.shop/public/${addressInput.image}`} alt="" />
                        <div class="invalid-feedback">{errorList.image}</div>
                        {errorImage && <div className='text-danger'>{errorImage}</div>}
                    </div>

                    <div class="col-md-12">
                        <label for="description" class="form-label">Description</label>
                        <textarea class="form-control" name='description' id="description" onChange={handleInput} value={addressInput.description} ></textarea>
                        <div class="text-danger">{errorList.description}</div>
                        
                    </div>

                    <div class="col-12">
                        <div class="form-check">
                        <input class="form-check-input " type="checkbox" name='featured' onChange={handleChecked} 
                            defaultChecked={allCheckbox.featured === 1 ? true:false}   />
                        <label class="form-check-label" >
                        Popular
                        </label>
                        
                        </div>
                    </div>
                    

                    

                    

                    <div class="col-12">
                        <button class="btn btn-primary" type="submit">Submit form</button>
                    </div>
                </form>
            </div>
            
        </div>
    );
}

export default EditAddress;
