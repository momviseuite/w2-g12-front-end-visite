import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import Swal from 'sweetalert2';
import { decryptData } from './CryptoJS';

const EditCategories = (props) => {
    const {id} = useParams();
    const navigate = useNavigate();
    const [loading, setLoading] = useState(true);
    
    const [categoryInput, setCategoryInput] = useState({
        name_place: '',
        image: '',
        description: '',
        status: '',
    })

    const [validation_error, setError] = useState([]);

    const [picture, setPicture] = useState([]);
    const[errorImage, setErrorImage] = useState(null);

    const handleImage = (e) =>{
        
        const selectedFile = e.target.files[0];
        const maxSize = 3 * 1024 * 1024; // 2MB
    
        if (selectedFile.size <= maxSize) {
            setPicture({image: e.target.files[0]});
            setErrorImage(null);
        } else {
            setErrorImage(' maximum size of 3MB.');
        }
    }

    const handleInput = (e) => {
        e.preventDefault();
        setCategoryInput({...categoryInput, [e.target.name]: e.target.value});
    }

    const [allCheckbox, setAllCheckbox] = useState([]);
    const handleChecked = (e) =>{
        e.persist();
        const { name, checked } = e.target;
        setAllCheckbox({
            ...allCheckbox,
            [name]: checked,
        });

    }

    useEffect(() =>{
        axios.get(`api/edit-category/${id}`).then(res=>{
            if(res.data.status === 200){
                
                setCategoryInput(res.data.category);
            }
            
            else if(res.data.status === 400){
                Swal.fire({
                    title: "Warning",
                    text: res.data.message,
                    icon: "warning"
                });
                navigate("/admin/view-categories");
            }
            setLoading(false);
        });


    },[id, navigate]);

    const handleUpdate = (e) => {
        e.preventDefault();

        const formData =  new FormData();
        formData.append('name_place', categoryInput.name_place);
        formData.append('image', picture.image);
        formData.append('description', categoryInput.description);
        formData.append('status', categoryInput.status ? 1 : 0);

            axios.post(`api/update-category/${id}`, formData,
                {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }
            ).then(res => {
                if(res.data.status === 200){
                    
                    Swal.fire({
                        title: "Success",
                        text: res.data.message,
                        icon: "success"
                    });
                    setError([]);
                    
                }
                else if(res.data.status === 400){
                    setError(res.data.validation_error);

                }
                else if(res.data.status === 404){
                    Swal.fire({
                        title: "Error",
                        text: res.data.message,
                        icon: "error"
                    });
                    navigate("/admin/view-categories");
                }
            })

    }

    if(loading){
        return (
            <>
                <h4>Loading edit Category...</h4>
            </>
        )
    } 

   
    return (
        <div>
            <div className="container-fluid">
                <form onSubmit={handleUpdate} className='row g-2'>
                    <div class="col-md-6">
                        <label for="slug" class="form-label">Slug Name</label>
                        <input type="text" name="name_place" onChange={handleInput} value={categoryInput.name_place} class="form-control" />
                        <span className='text-danger'>{validation_error.name_place} </span>
                    </div>

                    <div class="col-md-6">
                        <label for="image" class="form-label">image:</label>
                        <input type="file" id='image' name='image' onChange={handleImage}  class="form-control" />
                        <img className='mt-2' width={80} height={80} src={`https://visit-place.shop/public/${categoryInput.image}`} alt="" />
                        <div class="invalid-feedback">{validation_error.image}</div>
                        {errorImage && <div className='text-danger'>{errorImage}</div>}
                    </div>
                    
                    <div class="col-md-12">
                        <label for="description" class="form-label">Discriptions</label>
                        <textarea name='description' class="form-control" onChange={handleInput} value={categoryInput.description} id="description" rows="3"></textarea>
                        <span className='text-danger'>{validation_error.description} </span>
                    </div>

                    <div class="mb-3 me-1 form-check">
                        <input  class="form-check-input"
                            type="checkbox" 
                            name='status'
                            onChange={handleChecked} 
                            defaultChecked={allCheckbox.status === 1 ? true:false} 
                        />
                        <label class="form-check-label" for="exampleCheck1">Check me out</label>
                    </div>
                    <div class="col-12">
                        <button class="btn btn-primary" type="submit">Submit form</button>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default EditCategories;
