import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import Swal from 'sweetalert2';

const Category = () => {
    const navigate = useNavigate();

    const [categoryInput, setCategoryInput] = useState({
        name_place: '',
        image: '',
        description: '',
        status: '',
        error_list: [],
    })
    const [picture, setPicture] = useState([]);

    const handleInput = (e) => {
        e.persist();
        setCategoryInput({...categoryInput, [e.target.name]: e.target.value});
    }

    const[errorImage, setErrorImage] = useState(null);
    const handleImage = (e) =>{
        
        const selectedFile = e.target.files[0];
        const maxSize = 3 * 1024 * 1024; // 2MB
    
        if (selectedFile.size <= maxSize) {
            setPicture({image: e.target.files[0]});
            setErrorImage(null);
        } else {
            setErrorImage(' maximum size of 3MB.');
        }
    }

    const handleSubmit = (e) => {
        e.preventDefault();

        
        
        const formData =  new FormData();
        formData.append('name_place', categoryInput.name_place);
        formData.append('image', picture.image);
        formData.append('description', categoryInput.description);
        formData.append('status', categoryInput.status ? 1 : 0);

            axios.post('/api/add-category', formData,
            {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }
            ).then(res => {
                if(res.data.status === 200){
                    
                    Swal.fire({
                        title: "Success",
                        text: res.data.message,
                        icon: "success"
                    });
                }
                else if(res.data.status === 400){
                    setCategoryInput({...categoryInput, error_list:res.data.validation_error});

                }
            })

    }



    return (
        <div>
            <div className="container-fluid">
                <form onSubmit={handleSubmit} className='row g-2'>
                    <div class="col-md-6">
                        <label for="slug" class="form-label">Slug Name</label>
                        <input type="text" name="name_place" onChange={handleInput} value={categoryInput.name_place} class="form-control" />
                        <span className='text-danger'>{categoryInput.error_list.name_place} </span>
                    </div>

                    <div class="col-md-6">
                        <label for="image" class="form-label">image:</label>
                        <input type="file" id='image' name='image' onChange={handleImage}  class="form-control" required />
                        <div class="invalid-feedback">{categoryInput.error_list.image} </div>
                        {errorImage && <div className='text-danger'>{errorImage}</div>}
                    </div>
                    
                    <div class="col-md-12">
                        <label for="description" class="form-label">Discriptions</label>
                        <textarea name='description' class="form-control" onChange={handleInput} value={categoryInput.description} id="description" rows="3"></textarea>
                        <span className='text-danger'>{categoryInput.error_list.description} </span>
                        
                    </div>

                    {/* <div class="mb-3 me-1 form-check">
                        <input type="checkbox" class="form-check-input" id="exampleCheck1"/>
                        <label class="form-check-label" for="exampleCheck1">Check me out</label>
                    </div> */}
                    
                    
                    <div class="col-12">
                        <button type="submit" class="btn btn-primary float-end">Add</button>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default Category;
