import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';

const ViewAddress = () => {
    const [loading, setLoading] = useState(true);
    const [addressList, setAddressList] = useState({
        name: '',
        category_id: '',
        province_id: '',
        image: '',
        type_address: '',
        key_points: '',
        content: '',
        gallery: '',
        description: '',
        featured: '',
        status: '',
        
    });
    const navigate = useNavigate();




    useEffect(() =>{
        axios.get('api/view-address').then(res=>{
            if(res.status === 200){
                setAddressList(res.data.message);                
            }
            setLoading(false);
        })


    },[]);
    
    

    const handleDelete = (e, id) => {
        e.preventDefault();

        
        const confirm = window.confirm("Would you like to Delete?");
        if(confirm){
            axios.delete(`api/delete-address/${id}`).then(res => {
                if(res.data.status === 200){
                    window.location.reload();

                }else if(res.data.status === 400){
                    navigate("/admin/view-address");
                }
                
            }).catch(err => console.log(err));
        }
        
    }
    

  
    

    


    var ViewCategory_HTMLTABLE = "";
    if(loading){
        return (
            <>
                <h4>Loading Category...</h4>
            </>
        )
    }   
    else{
        ViewCategory_HTMLTABLE =
        addressList?.map((item, index) => {
                return (
                    <tr key={item.id}>
                        <td>{item.name}</td>
                        <td>
                        
                        
                            <Link className='box-img-property'>
                                <img className='img-list-property' src={`https://visit-place.shop/public/${item.image}`} alt="" />
                            </Link>
                        </td>
                        <td>{item.category.name_place}</td>
                        <td>{item.province.name}</td>
                        <td>{item.status}</td>
                        <td>{item.featured}</td>
                        <td>
                            <button className='btn btn-primary me-2 mt-1'>
                                <Link to={`/admin/edit-address/${item.id}`} className='text-white'>Edit</Link>
                            </button>

                            <button id='e_delete' className='btn btn-danger mt-1' onClick={e => handleDelete(e, item.id)}>Delete</button>
                        </td>
                    </tr>
                )
        })
    }

    return (
        <div>
            <div className="container-fluid">
                <h1 class="h3 mb-2 text-gray-800">Tables</h1>
                <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below.
                    For more information about DataTables, please visit the <a target="_blank"
                        href="https://datatables.net">official DataTables documentation</a>.</p>

                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered text-center" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Imgage</th>
                                        <th>Category</th>
                                        <th>Province</th>
                                        <th>Ststus</th>
                                        <th>Popular</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                
                                <tbody >
                                    {ViewCategory_HTMLTABLE}
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            
            </div>
        </div>
    );
}

export default ViewAddress;
