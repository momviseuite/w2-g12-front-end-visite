import React, { useEffect, useState } from 'react';
import ReactPaginate from 'react-paginate';
import { Link } from 'react-router-dom';

const Images = (props) => {
    const {data} = props;

    const [currentItems, setCurrentItems] = useState([]);
    const [pageCount, setPageCount] = useState(0);
    const [itemOffset, setItemOffset] = useState(0);
    const itemsPerPage = 10;

    useEffect(() =>{
        const endOffset = itemOffset + itemsPerPage;
        setCurrentItems(data.slice(itemOffset, endOffset));
        setPageCount(Math.ceil(data.length / itemsPerPage));
    }, [itemOffset,itemsPerPage,data]);

    const handlePageClick = (event) => {
        const newOffset = (event.selected * itemsPerPage) % data.length;
        setItemOffset(newOffset);
    };

    return (
        <>
            <div className="images container text-center">
                <div class="row row-cols-2 row-cols-lg-5 g-2 g-lg-3">
                    {currentItems.map(item => {
                        return (
                            <div class="col box-images">
                                <div class="box-add text-center">
                                {/* <Link to={`/demo_address/${item.id}`}> */}
                                    <Link to={`/Demo_address/${item.id}/${item.name}/${item.category.id}/${item.category.name_place}`}>
                                        <span className='add-text text-white'>{item.name}</span>
                                        <img src={`https://visit-place.shop/public/${item.image}`} class="col-img card-img-top W-100 h-100 " alt="..." />
                                    </Link>
                                </div>
                            </div>
                        )
                    })}

                    </div>
                </div>


            <ReactPaginate
                breakLabel="..."
                nextLabel="next >"
                onPageChange={handlePageClick}
                pageRangeDisplayed={5}
                pageCount={pageCount}
                previousLabel="< previous"
                renderOnZeroPageCount={null}
                containerClassName='pagination'
                pageClassName='page-num'
                nextLinkClassName='page-num'
                activeClassName='active'
            />
        </>
    );
}

export default Images;
