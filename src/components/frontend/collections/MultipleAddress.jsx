import axios from 'axios';
import React, { useEffect, useState } from 'react';
import ReactPaginate from 'react-paginate';
import { useParams } from 'react-router-dom';
import Images from './Images';

// import pagination from '../../../assets/pagination';


const MultipleAddress = () => {

    const {category_id} = useParams();

   

const {name} = useParams();
    const [loading, setLoading] = useState(true);
    const [addressList, setAddressList] = useState([]);

    useEffect(() =>{
        axios.get(`api/front/multi-address/${category_id}`).then(res=>{
            if(res.data.status === 200){
                setAddressList(res.data.message);
            }
            setLoading(false);
        }).catch(error => {
            console.error('Error fetching categories:', error);
        });

    },[category_id]);

    if(loading){
        return (
            <>
                <h4>Loading Category...</h4>
            </>
        )
    }


    return (
        <div>
            <section className='container mt-3'>
                <h4 className='text-center'>{name}</h4>
                <Images data={addressList} />
            </section>       
        </div>
    );
}

export default MultipleAddress;
