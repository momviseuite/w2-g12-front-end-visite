import axios from 'axios';
import { resetWarningCache } from 'prop-types';
import React, {useState, useEffect} from 'react';
import {useLocation, useNavigate} from "react-router-dom";
import Swal from 'sweetalert2';

function GoogleCallback() {

    // const [loading, setLoading] = useState(true);
    const [data, setData] = useState({});
    const [user, setUser] = useState(null);
    const location = useLocation();
    const navigate = useNavigate();

    useEffect(() => {
        // axios.get('/sanctum/csrf-cookie').then(response => {
            axios.get(`https://visit-place.shop/public/api/auth/google/callback${location.search}`).then(res => {
                if(res.data.status === 200){
                    Swal.fire({
                        title: "Success",
                        text: res.data.message,
                        icon: "success"
                    });
                    navigate("/");
                    localStorage.setItem('auth_token', res.data.token);
                    localStorage.setItem('auth_name', res.data.username);
                }
                
            })
        // });
    }, []);  
}


export default GoogleCallback;