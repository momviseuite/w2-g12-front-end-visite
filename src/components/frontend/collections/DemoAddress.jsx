import React, { useEffect, useState } from 'react';
import "../../../assets/css/view_detail.css";
import axios from 'axios';
import { Link, useNavigate, useParams } from 'react-router-dom';
import Swal from 'sweetalert2';
import Rating from 'react-rating';
import Ratings from './Ratings';
import Modal from './Modal';
import Slider from 'react-slick';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";


const DemoProperties = ({userId}) => {

    var settings = {
        dots: true,
        infinite: false,
        speed: 500,
        slidesToShow: 6,
        slidesToScroll: 6,
        initialSlide: 0,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              initialSlide: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
    };

    const navigate = useNavigate();
    const {id,category_id} = useParams();   
    const [loading, setLoading] = useState(true); 
    const [ratings, setRatings] = useState([]);
    const [addressList, setAddressList] = useState([]);
    const [related, setRelated] = useState([]);
    const [location, setLocation] = useState([]);
    const [rating, setRating] = useState(0);
    const [comments, setComments] = useState([]);
    const [replies, setReplies] = useState({});
    const [comment, setComment] = useState('');
    const [reply, setReply] = useState('');
    const [error, setError] = useState([]);

    const takeRelated = related.slice(0, 8);

    const [showModal, setShowModal] = useState(false);
    const [modalTitle, setModalTitle] = useState('');
    
    const [address, setAddress] = useState(null);
    
    const handleCommentSubmit = async (e) => {
        e.preventDefault();
        try {
            
            axios.post('/api/comments', {
                user_id: userId,
                address_id: id,
                body: comment
            }).then(res => {
                if (res.data.status === 200) {
                    Swal.fire({
                        title: "Success",
                        text: res.data.message,
                        icon: "success"
                    });
                    window.location.reload();
        
                    setComment('');
                    setComments([...comments, res.data]);
        
                } else if (res.data.status === 404) {
                    const modal = new bootstrap.Modal(document.getElementById('exampleModal'));
                    modal.show();
                }
                else if(res.data.status === 405){
                    setError(res.data.validation_error);

                }
        
            })
            
        } catch (error) {
            console.error(error);
        }
    };
    
    const handleRatingChange = (value) => {
        
        axios.post('/api/ratings', {
            address_id: id,
            rating: value
        }).then(res => {
            if(res.data.status === 200){
                Swal.fire({
                    title: "Success",
                    text: res.data.message,
                    icon: "success"
                });
                setRating(value);
            }else if(res.data.status === 404){
                const modal = new bootstrap.Modal(document.getElementById('exampleModal'));
                modal.show();
            }
        }).catch(error => {
            console.error(error);
        });

    }

    // wishlist
    const [inWishlist, setInWishlist] = useState(false);
    const handleWishlistToggle = async () => {
        try {
            const url = inWishlist ? '/api/wishlist/remove' : '/api/wishlist/add';
            const response = await axios.post(url, { address_id: id,
             });
    
            if (response.data.status === 200) {
                Swal.fire({
                    title: "Success",
                    text: response.data.message,
                    icon: "success"
                });
                window.location.reload();
    
                const wishlist = JSON.parse(localStorage.getItem('wishlist')) || {};
                if (inWishlist) {
                    delete wishlist[id];
                } else {
                    wishlist[id] = true;
                }
                localStorage.setItem('wishlist', JSON.stringify(wishlist));
                setInWishlist(!inWishlist);
    
            } else if (response.data.status === 404) {
                const modal = new bootstrap.Modal(document.getElementById('exampleModal'));
                modal.show();
            }
    
        } catch (error) {
            console.error('Error updating wishlist', error);
        }
    };
    
    useEffect(() =>{     
            const savedWishlist = JSON.parse(localStorage.getItem('wishlist')) || {};
            setInWishlist(savedWishlist[id] || false);

            axios.get(`api/front/view-address/${id}`).then(res=>{
                if(res.data.status === 200){
                    setAddressList(res.data.message);
                    setLoading(false)
                }
                else if(res.data.status === 400){
                    Swal.fire({
                        title: "Warning",
                        text: res.data.message,
                        icon: "warning"
                    });
                    navigate("/");
                }
                else if(res.data.status === 404){
                    Swal.fire({
                        title: "Warning",
                        text: res.data.message,
                        icon: "warning"
                    });
                    navigate("/");
                }
            });

            // comments
            axios.get(`api/comments/${id}`).then(res=>{
                if(res.data.status === 200){
                    setComments(res.data.message);
                }
                setLoading(false);
            })

            // ratting
            axios.get('/api/ratings')
            .then(response => {
                setRatings(response.data);
            })
            .catch(error => {
                console.error(error);
            });

            // related place address
            axios.get(`api/front/multi-address/${category_id}`).then(res=>{
                if(res.data.status === 200){
                    setRelated(res.data.message);
                }
                setLoading(false);
            }).catch(error => {
                console.error('Error related place:', error);
            });

            // count_views
            axios.get(`/api/address/${id}`)
            .then(response => {
                setAddress(response.data);
            })
            .catch(error => {
                console.error('There was an error fetching the property data!', error);
            });

    },[id,category_id]);

    return (
        <div className=''>
            <div class="blog-single gray-bg">
                <div class="container">
                    <div class="row align-items-start">
                        <div class="col-lg-8 m-15px-tb">
                            {
                                loading ? (
                                    <>
                                        <span class="placeholder col-6"></span>
                                        <span class="placeholder w-75"></span>
                                        <span class="placeholder w-100"></span>
                                        <span class="placeholder w-100"></span>
                                        <span class="placeholder" width={25}></span>
                                    </>
                                ): (
                                    <>
                                    
                                        {
                                            addressList.map((item) => {
                                                return (
                                                    <article class="article" key={item.id}>
                                                        <div class="article-img">
                                                            <span className="view-post">{item.count_views} <i class="fa-solid fa-eye mx-1 view"></i></span>
                                                            
                                                        <img src={`https://visit-place.shop/public/${item.image}`} class="w-100" height={350} alt="..." />
                                                        <div className="wishlist">
                                                            <i className={`wishlist-icon ${inWishlist ? 'in-wishlist' : ''}`}
                                                                    onClick={handleWishlistToggle}>
                                                                    {inWishlist ? '♥' : '♡'}
                                                            </i>
                                                        </div>

                                                        </div>
                                                        <div class="article-title">
                                                            <h6><a href="#">Lifestyle</a></h6>
                                                            <h2>{item.name}</h2>
                                                            
                                                            <Rating
                                                                initialRating={rating}
                                                                onClick={handleRatingChange}
                                                                emptySymbol="far fa-star "
                                                                fullSymbol="fas fa-star text-warning"
                                                                rating={rating}
                                                                className=''
                                                                
                                                            />
                                                            
                                                            
                                                        </div>
                                                        <div class="article-content">
                                                            <p>Aenean eleifend ante maecenas pulvinar montes lorem et pede dis dolor pretium donec dictum. Vici consequat justo enim. Venenatis eget adipiscing luctus lorem. Adipiscing veni amet luctus enim sem libero tellus viverra venenatis aliquam. Commodo natoque quam pulvinar elit.</p>
                                                            <p>Eget aenean tellus venenatis. Donec odio tempus. Felis arcu pretium metus nullam quam aenean sociis quis sem neque vici libero. Venenatis nullam fringilla pretium magnis aliquam nunc vulputate integer augue ultricies cras. Eget viverra feugiat cras ut. Sit natoque montes tempus ligula eget vitae pede rhoncus maecenas consectetuer commodo condimentum aenean.</p>
                                                            <h4>What are my payment options?</h4>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                                            <blockquote>
                                                                <p class="blockquote-footer">Someone famous in <cite title="Source Title">Dick Grayson</cite></p>
                                                            </blockquote>
                                                        </div>
                                                        
                                                    </article>
                                                    
                                                )
                                            })
                                        }

                                        <div class="contact-form mt-4 article-comment">
                                          <div>
                                              <form onSubmit={handleCommentSubmit}>

                                                  <div class="form-floating">
                                                      <textarea type="text" value={comment} onChange={(e) => setComment(e.target.value)} class="form-control" placeholder="Leave a comment here" id="floatingTextarea"></textarea>
                                                      <label for="floatingTextarea">Comments</label>
                                                      <span className='text-danger'>{error.body} </span>
                                                  </div>
                                                  <div class="send comment-btn mt-2">
                                                      <button type="submit " class="px-btn theme"><span>Submit <i class="fa-solid fa-arrow-right"></i></span> </button>
                                                  </div>

                                              </form>
                                              {
                                              comments.map((item) => {
                                                  return (
                                                      <div key={item.id}>
                                                          <div class="card p-3 mt-2">
                                                              <div class="d-flex justify-content-between align-items-center">
                                                                  <div class="user d-flex flex-row align-items-center">
                                                                      <img src="https://i.imgur.com/C4egmYM.jpg" width="30" class="user-img rounded-circle mr-2"/>
                                                                      <span><small class="font-weight-bold text-primary">{item.user.name}</small></span>
                                                                  </div>
                                                                  <small>{new Date(item.created_at).toLocaleString()}</small>
                                                              </div>

                                                              <div class="action d-flex justify-content-between mt-2 align-items-center">
                                                                  <div class="reply px-4">
                                                                      <small class="font-weight-bold">{item.body}</small>
                                                                  </div>

                                                                  
                                                              </div>
                                                          </div>

                                                      </div>
                                                  )
                                              })
                                              }
                                              
                                          </div>
                                        </div>
                                    </>
                                )
                            }
                            
                            

                        </div>

                        <div class="col-lg-4 m-15px-tb blog-aside">
                            <div class="widget widget-latest-post">
                                <div class="widget-title">
                                    <h3>Related Place</h3>
                                </div>
                                <div class="widget-body">
                                    {
                                        loading ? (
                                            <>
                                                <span class="placeholder col-6"></span>
                                                <span class="placeholder w-75"></span>
                                                <span class="placeholder w-100"></span>
                                                <span class="placeholder" width={25}></span>
                                            </>
                                        ): (
                                            <>
                                                {
                                                    takeRelated.map((item, index) => {
                                                        return (
                                                            <div class="latest-post-aside media">
                                                                <div class="lpa-left media-body">
                                                                    <div class="lpa-title">
                                                                        <h5><a href={`/Demo_address/${item.id}/${item.name}/${item.category.id}/${item.category.name_place}`}>{item.name}</a></h5>
                                                                    </div>
                                                                    <div class="lpa-meta">
                                                                        <span>
                                                                        </span>&emsp;
                                                                        <span class="date" href="#">
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                <div class="lpa-right">
                                                                    <a href={`/Demo_address/${item.id}/${item.name}/${item.category.id}/${item.category.name_place}`}>
                                                                        <img src={`https://visit-place.shop/public/${item.image}`} class="" width={400} height={60} alt="..." />
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        )
                                                    })
                                                }
                                            </>
                                        )
                                    }
                                    
                                </div>
                            </div>

                            

                        </div>
                    </div>
                    <h3 className="text-center mt-3">Recommend Place</h3>
                    {/* ======= */}
                    <section className='container slider-property'>
                
                        <div className="row text-center d-flex justify-content-center">
                            {loading ? (
                                <div class="card" aria-hidden="true">
                                    <img src="..." class="card-img-top" alt="..."/>
                                    <div class="card-body">
                                        <h5 class="card-title placeholder-glow">
                                            <span class="placeholder col-6"></span>
                                        </h5>
                                        <p class="card-text placeholder-glow">
                                            <span class="placeholder col-7"></span>
                                            <span class="placeholder col-4"></span>
                                            <span class="placeholder col-4"></span>
                                            <span class="placeholder col-6"></span>
                                            <span class="placeholder col-8"></span>
                                        </p>
                                        <a class="btn btn-primary disabled placeholder col-6" aria-disabled="true"></a>
                                    </div>
                                </div>
                            ) : (
                                <Slider {...settings} class="row row-cols-2 row-cols-lg-5 g-2 g-lg-3  bg-black"> 
                                    {
                                            related.map((item) => {
                                                return (
                                                    <div class="col">
                                                        <div class="box-add text-center">
                                                            <a href={`/Demo_address/${item.id}/${item.name}/${item.category.id}/${item.category.name_place}`}>
                                                                <span className='add-text text-white'>{item.name}</span>
                                                                <img src={`https://visit-place.shop/public/${item.image}`} class="col-img card-img-top W-100 h-100 " alt="..." />
                                                            </a>
                                                        </div>
                                                    </div>
                                                )
                                            })
                                        }
                                </Slider>
                            )}


                    
                        </div>
                    
                    </section>
                </div>
            </div>
            <div class="toast-container position-fixed bottom-0 end-0 p-3">
                <div id="liveToast" class="toast" role="alert" aria-live="assertive" aria-atomic="true">
                    <div class="toast-header">
                    <img src="..." class="rounded me-2" alt="..."/>
                    <strong class="me-auto">Bootstrap</strong>
                    <small>11 mins ago</small>
                    <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
                    </div>
                    <div class="toast-body">
                    Hello, world! This is a toast message.
                    </div>
                </div>
            </div>
            <Modal/>
        </div>
    );
}

export default DemoProperties;
