import axios from 'axios';
import React, {useState, useEffect} from 'react';

function SignIn() {

    const [loginUrl, setLoginUrl] = useState(null);

    useEffect(() => {
        fetch('https://visit-place.shop/public/api/auth/google/url', {
            headers : {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        })
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                throw new Error('Something went wrong!');
            })
            .then((data) => setLoginUrl( data.url ))
            .catch((error) => console.error(error));
    }, []);

    return (
        <span>
            {loginUrl != null && (
                <a href={loginUrl} className='text-white'>Sign In With Google</a>
            )}
        </span>
    );
}

export default SignIn;