import React, { useState } from 'react';

const Modal = () => {

    // login --------------
    const [loginInput, setLogin] = useState({
        email: '',
        password: '',
        error_list: [],
    })
    const handleInput = (e) => {
        e.preventDefault();
        setLogin({...loginInput, [e.target.name]: e.target.value});
    }
    const handleSubmit = (e) => {
        e.preventDefault();
  
        const data = {
            email: loginInput.email,
            password: loginInput.password,
        }
  
        // axios.get('/sanctum/csrf-cookie').then(response => {
            axios.post('api/login', data).then(res => {
                if(res.data.status === 200){
                    localStorage.setItem('auth_token', res.data.token);
                    localStorage.setItem('auth_name', res.data.username);
                    Swal.fire({
                        title: "Success",
                        text: res.data.message,
                        icon: "success"
                    });
                    if(res.data.role === 'admin'){
                      navigate("/admin/");

                    }
                    else{
                      navigate("/");
                    }
                }
                else if(res.data.status === 400){
                  Swal.fire({
                    title: "Warning",
                    text: res.data.message,
                    icon: "warning"
                });
  
                }
                else{
                    setLogin({...loginInput, error_list:res.data.validation_error});
                }
            })
        // });
  
    }
    // end login --------------

    return (
        <>
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="exampleModalLabel">Please Login</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form action="" onSubmit={handleSubmit}>
                            <div class="form-group row">
                                <label for="email" class="col-sm-3 col-xs-3 col-form-label">Email <span className='text-danger'>*</span></label>
                                <div class="col-sm-9 col-xs-9">
                                    <input type="email" name="email" onChange={handleInput} value={loginInput.email} class="form-control" id="email" placeholder="email" />
                                    <span className='text-danger'>{loginInput.error_list.email} </span>
                                </div>
                                </div>

                                <div class="form-group row mt-3">
                                <label for="inputPassword" class="col-sm-3 col-xs-3 col-form-label">Password <span className='text-danger'>*</span></label>
                                <div class="col-sm-9 col-xs-9">
                                    <input type="password" name='password' onChange={handleInput} value={loginInput.password} class="form-control" id="inputPassword" placeholder="password" />
                                    <span className='text-danger'>{loginInput.error_list.password} </span>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <a href="/register">register</a>
                                <button type="button" class="btn btn-primary">Log In</button>
                            </div>
                        </form>
                    </div>
                    
                    </div>
                </div>
            </div>
        </>
    );
}

export default Modal;
