import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom';

function ViewFavorite() {
    const [favorite , setFavorite] = useState([]);
    const [loading, setLoading] = useState(true);
    

    useEffect(() =>{
        // api category
        axios.get('api/wishlist').then(res => {
            if(res.data.status === 200){
                setFavorite(res.data.message);
            }
            setLoading(false);
        })
        .catch(error => {
            console.error('Error fetching categories:', error);
        });

    },[])

    // const removeFromWishlist = (addressId) => {
      
    //     axios.delete(`api/wishlist/${addressId}`).then(res => {
    //         if (res.data.status === 200){
    //             alert(1);
    //         }
    //     });
         
        
    // };

    // const handleRemoveFromWishlist = async (addressId) => {
        
    //     removeFromWishlist(addressId);
    //     fetchWishlist();
    // };
    // }

    const handleDelete = (e, id) => {
        e.preventDefault();
        const confirm = window.confirm("Would you like to Delete?");
        if(confirm){
            axios.delete(`api/wishlist/${id}`).then(res => {
                    localStorage.removeItem('wishlist');
                    window.location.reload();
            }).catch(err => console.log(err));
        }
        
    }


    if(loading){
        return (
            <div className="text-center">
                <h4>Loading...</h4>
            </div>
        )
    }

  return (
    <>
        {/* <div class="container fav flex mx-auto w-full items-center justify-center mt-4">
            <ul class="flex flex-col  p-4">
                {
                    favorite.map((item,index) =>{
                        return (
                                <li class="border-gray-400 flex flex-row mb-2">
                                    <div class="select-none cursor-pointer bg-gray-200 rounded-md flex flex-1 items-center p-4  transition duration-500 ease-in-out transform hover:-translate-y-1 hover:shadow-lg">

                                        <div class="flex-1 pl-1 mr-16">
                                            <div class="font-medium">
                                                <img src={`https://visit-place.shop/public/${item.address.image}`} class="card-img-top box-pro" alt="..." />
                                            </div>
                                            <div class="text-gray-600 text-sm">200ml</div>
                                        </div>
                                        <div class="text-gray-600 text-xs">6:00 AM</div>
                                    </div>
                                    <button onClick={() => handleRemoveFromWishlist(item.address_id)}>Remove</button>
                                </li>
                        )
                    })
                }
            </ul>
        
        </div> */}

        <div className='container'>
            {
            favorite.map((item) => {
                return (
                    <div key={item.id}>
                        <div class="card p-3 mt-2">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="user d-flex flex-row align-items-center">
                                <img src={`https://visit-place.shop/public/${item.address.image}`} width={80} height={80} class="" alt="..." />
                                    <small class="font-weight-bold text-primary p-3">
                                        {item.address.name}
                                    </small>
                                </div>
                                <small>
                                {/* <button onClick={() => handleRemoveFromWishlist(item.address_id)}>Remove</button> */}
                                <button id='e_delete' className='btn btn-danger mt-1' onClick={e => handleDelete(e, item.address_id)}>Delete</button>
                                </small>
                            </div>

                            <div class="action d-flex justify-content-between mt-2 align-items-center">
                                <div class="reply px-4">
                                    <small class="font-weight-bold">{item.body}</small>
                                </div>

                                
                            </div>
                        </div>

                    </div>
                )
            })
            }
        </div>
    </>
  )
}

export default ViewFavorite