import React, { useEffect, useState } from 'react';
import home from '../../images/home.jpg';
import Slider from 'react-slick';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import axios from 'axios';
import Navbar from './Navbar';
import { Link } from 'react-router-dom';
import Modal from './collections/Modal';
import Ratings from './collections/Ratings';


const Home = () => {

    var settings = {
        dots: true,
        infinite: false,
        speed: 500,
        slidesToShow: 6,
        slidesToScroll: 6,
        initialSlide: 0,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              initialSlide: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
    };

    // view-property
    const [loading, setLoading] = useState(true);
    const [category, setGategory] = useState([]);
    const [addressList, setAddressList] = useState([]);
    const [latestAddress, setLatestAddress] = useState([]);
    const [ratings, setRatings] = useState([]);


    useEffect(() =>{

        // api category
        axios.get('api/front/category').then(res => {
            if(res.data.status === 200){
                setGategory(res.data.category);
            }
            setLoading(false);
        })
        .catch(error => {
            console.error('Error fetching categories:', error);
        });

        // api address
        axios.get('api/front/view-address').then(res => {
            if(res.data.status === 200){
                setAddressList(res.data.message);
            }
            setLoading(false);
        })
        .catch(error => {
            console.error('Error fetching categories:', error);
        });

        // latest address
        axios.get('api/front/view-latestAddress').then(res => {
            if(res.data.status === 200){
                setLatestAddress(res.data.message);
            }
            setLoading(false);
        })
        .catch(error => {
            console.error('Error fetching categories:', error);
        });

        // ratting
        axios.get('/api/ratings')
            .then(response => {
                setRatings(response.data);
            })
            .catch(error => {
                console.error(error);
            });

    },[]);

    if(loading){
        return (
            <>
                <h4>Loading ...</h4>
            </>
        )
    } 

    

    
    return (
        <div className=''>
            

            <section className='container home-slider'>
                <div className="border-nav  m-auto">
                </div>
                <div className="row m-auto ">
                    <div className="col-6 plus">+</div>
                    <div className="col-6 plus home-plus">+</div>
                </div>
                

                <div className="box-slider m-auto">
                    <div className="row box-text-home">
                        <span className='text-home'>
                            We mark your dream come true 
                        </span>
                    </div>
                    {/* <img className="" src={home} alt="" /> */}
                    <img src="https://visit-place.shop/public/uploads/category/1714099938.jpg" alt="" />
                </div>
                <div className="row m-auto ">
                    <div className="col-6 plus">+</div>
                    <div className="col-6 plus home-plus">+</div>
                </div>
                <div className="border-nav  m-auto">
                </div>
            </section>

            <section className='container slider-property'>
                
                <div className="row text-center d-flex justify-content-center">

                <h3>Category</h3>
                    {loading ? (
                        <div class="card" aria-hidden="true">
                            <img src="..." class="card-img-top" alt="..."/>
                            <div class="card-body">
                                <h5 class="card-title placeholder-glow">
                                    <span class="placeholder col-6"></span>
                                </h5>
                                <p class="card-text placeholder-glow">
                                    <span class="placeholder col-7"></span>
                                    <span class="placeholder col-4"></span>
                                    <span class="placeholder col-4"></span>
                                    <span class="placeholder col-6"></span>
                                    <span class="placeholder col-8"></span>
                                </p>
                                <a class="btn btn-primary disabled placeholder col-6" aria-disabled="true"></a>
                            </div>
                        </div>
                    ) : (
                        <Slider {...settings} class="row row-cols-2 row-cols-lg-5 g-2 g-lg-3  bg-black"> 
                            {
                                    category?.map((item) => {
                                        return (
                                            <div class="col">
                                                <div class="box-add text-center">
                                                    <Link to={`/multi_address/${item.id}/${item.name_place}`}>
                                                    <span className='add-text text-white'>{item.name_place}</span>
                                                    <img src={`https://visit-place.shop/public/${item.image}`} class="col-img card-img-top W-100 h-100 " alt="..." />
                                                    </Link>
                                                </div>
                                            </div>
                                        )
                                    })
                                }
                        </Slider>
                    )}


               
                </div>
            
            </section>

            <section className='container mt-5 most-property'>
                <h3>Most Popular</h3>
                <div class=" ">
                    <div class="row row-cols-1 row-cols-lg-4 g-2 g-lg-3">
                    {loading ? (
                        <div class="card" aria-hidden="true">
                            <img src="..." class="card-img-top" alt="..."/>
                            <div class="card-body">
                                <h5 class="card-title placeholder-glow">
                                    <span class="placeholder col-6"></span>
                                </h5>
                                <p class="card-text placeholder-glow">
                                    <span class="placeholder col-7"></span>
                                    <span class="placeholder col-4"></span>
                                    <span class="placeholder col-4"></span>
                                    <span class="placeholder col-6"></span>
                                    <span class="placeholder col-8"></span>
                                </p>
                                <a class="btn btn-primary disabled placeholder col-6" aria-disabled="true"></a>
                            </div>
                        </div>
                    ) : (
                     <>
                        {
                            addressList?.map((item) => {
                                return (
                                    <div class="col col-pos">
                                        <div className="box-card"></div>
                                        <div class="card border-black" key={item.id}>
                                            {/* <span className='text-type m-1'>{item.type.name}</span> */}
                                            <img src={`https://visit-place.shop/public/${item.image}`} class="card-img-top box-pro" alt="..." />
                                            {/* <WishlistButton property_id={item.id} initialInWishlist={item.inWishlist} /> */}
                                            <div class="card-body">
                                                
                                                <h5 class="card-title">{item.name}</h5>
                                                {/* {ratings.filter(rating => rating.address_id === item.id).map(rating => (
                                                    <div key={rating.id}>{rating.id}
                                                        <div className="loc-pri-flex">
                                                            <p> Rating: ({rating.rating.toFixed(2)})</p>
                                                            <p><Ratings value={rating.rating} /> </p>
                                                        </div>
                                                        <hr className=' hr-size' />
                                                    </div>
                                                ))} */}
                                                <p class="card-text"></p>
                                                <Link to={`/Demo_address/${item.id}/${item.name}/${item.category_id}/${item.category.name_place}`} className=''>See Detail</Link>
                                            </div>
                                        </div>
                                        
                                    </div>
                                )
                            })
                        }
                     </>
                    )}
                    </div>
                </div>
            </section>

            <section className='container mt-5 most-property'>
                <h3>Most Latest</h3>
                <div class=" ">
                    <div class="row row-cols-1 row-cols-lg-4 g-2 g-lg-3">
                    {loading ? (
                        <div class="card" aria-hidden="true">
                            <img src="..." class="card-img-top" alt="..."/>
                            <div class="card-body">
                                <h5 class="card-title placeholder-glow">
                                    <span class="placeholder col-6"></span>
                                </h5>
                                <p class="card-text placeholder-glow">
                                    <span class="placeholder col-7"></span>
                                    <span class="placeholder col-4"></span>
                                    <span class="placeholder col-4"></span>
                                    <span class="placeholder col-6"></span>
                                    <span class="placeholder col-8"></span>
                                </p>
                                <a class="btn btn-primary disabled placeholder col-6" aria-disabled="true"></a>
                            </div>
                        </div>
                    ) : (
                     <>
                        {
                            latestAddress?.map((item) => {
                                return (
                                    <div class="col col-pos">
                                        <div className="box-card"></div>
                                        <div class="card border-black" key={item.id}>
                                            {/* <span className='text-type m-1'>{item.type.name}</span> */}
                                            <img src={`https://visit-place.shop/public/${item.image}`} class="card-img-top box-pro" alt="..." />
                                            {/* <WishlistButton property_id={item.id} initialInWishlist={item.inWishlist} /> */}
                                            <div class="card-body">
                                                
                                                <h5 class="card-title">{item.name}</h5>
                                                {/* {ratings.filter(rating => rating.property_id === item.id).map(rating => (
                                                    <div key={rating.id}>
                                                        <div className="loc-pri-flex">
                                                            <p> Rating: ({rating.rating.toFixed(2)})</p>
                                                            <p><Ratings value={rating.rating} /> </p>
                                                        </div>
                                                        <hr className=' hr-size' />
                                                    </div>
                                                ))} */}
                                                <p class="card-text"></p>
                                                <Link to={`/Demo_address/${item.id}/${item.name}/${item.category_id}/${item.category.name_place}`} className=''>See Detail</Link>
                                            </div>
                                        </div>
                                        
                                    </div>
                                )
                            })
                        }
                     </>
                    )}
                    </div>
                </div>
            </section>
            <Modal/>

        </div>
    );
}

export default Home;
