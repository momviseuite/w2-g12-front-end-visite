import NotFound from "../components/NotFound";
import Contact from "../components/frontend/Contact";
import Home from "../components/frontend/Home";
import MultipleAddress from "../components/frontend/collections/MultipleAddress";
import DemoAddress from "../components/frontend/collections/DemoAddress";
import ViewFavorite from "../components/frontend/collections/ViewFavorite";
import Addresses from "../components/frontend/collections/Addresses";




const routeFrontent = [
    { path: '*', exact: true, name: '*', component: NotFound },
    { path: '/', exact: true, name: 'Home', component: Home },
    { path: '/contact', exact: true, name: 'Contact', component: Contact },
    { path: '/allAddress', exact: true, name: 'allAddress', component: Addresses},


    { path: '/multi_address/:category_id/:name', exact: true, name: 'MultipleAddress', component: MultipleAddress},
    { path: '/Demo_address/:id/:name/:category_id/:name_place', exact: true, name: 'DemoAddress', component: DemoAddress},

    { path: '/viewFavorite', exact: true, name: 'ViewFavorite', component: ViewFavorite},


    ];
    
    
    export default routeFrontent;