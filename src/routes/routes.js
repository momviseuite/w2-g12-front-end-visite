import Address from "../components/admin/Address";
import Category from "../components/admin/Category";
import Dashboard from "../components/admin/Dashboard";
import EditAddress from "../components/admin/Edit_Address";
import EditCategories from "../components/admin/Edit_Categories";
import Profile from "../components/admin/Profile";
import ReviewComment from "../components/admin/ReviewComment";
import ReviewRating from "../components/admin/ReviewRating";
import TextEditor from "../components/admin/TextEditor";
import ViewAddress from "../components/admin/View_Address";
import ViewCategories from "../components/admin/View_Categories";
 

const routes = [
    { path: '/', exact: true, name: 'Admin' },
    { path: '/dashboard', exact: true, name: 'Dashboard', component: Dashboard},
    // { path: '/properties', exact: true, name: 'Properties', component: Properties},
    { path: '/add-categories', exact: true, name: 'Categorties', component: Category},
    { path: '/view-categories', exact: true, name: 'ViewCategories', component: ViewCategories},
    { path: '/edit-category/:id', exact: true, name: 'EditCategories', component: EditCategories},

    // Property
    { path: '/address', exact: true, name: 'Address', component: Address},
    { path: '/view-address', exact: true, name: 'ViewAddress', component: ViewAddress},
    { path: '/edit-address/:id', exact: true, name: 'EditAddress', component: EditAddress},

    { path: '/review-comment', exact: true, name: 'ReviewComment', component: ReviewComment},
    { path: '/review-rating', exact: true, name: 'ReviewRating', component: ReviewRating},


    { path: '/text', exact: true, name: 'TextEditor', component: TextEditor},


    { path: '/profile', exact: true, name: 'Profile', component: Profile},
    
    ];
    
    export default routes;